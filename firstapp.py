import sys
from PySide2.QtWidgets import QApplication, QMainWindow, QCalendarWidget

app = QApplication(sys.argv)

window = QMainWindow()
window.setWindowTitle("Ma première fenêtre")

calendar = QCalendarWidget(window)
window.setCentralWidget(calendar)

window.show()

sys.exit(app.exec_())
