#! /usr/bin/env python3

import socket, multiprocessing

NB_WORKERS = 10


def pong_server(host, port):
    with socket.socket(socket.AF_INET, socket.SOCK_STREAM) as s:
        s.setsockopt(socket.SOL_SOCKET, socket.SO_REUSEADDR, 1)
        s.bind((host, port))
        s.listen(1)
        with multiprocessing.Pool(NB_WORKERS) as pool:
            while True:
                conn, address = s.accept()
                pool.apply(handle, (conn, address))


def handle(conn, address):
    with conn:
        buff = conn.recv(512)
        print(buff.decode('utf-8'))
        conn.sendall("pong".encode('utf-8'))


if __name__ == "__main__":
    import argparse

    parser = argparse.ArgumentParser()
    parser.add_argument('-H', '--host', default='localhost', help="l'adresse ou le nom d'hôte du serveur")
    parser.add_argument('-p', '--port', type=int, default=8000, help="le port d'écoute du serveur")
    args = parser.parse_args()

    print(f"Lancement du serveur sur {args.host}:{args.port}")
    pong_server(args.host, args.port)

