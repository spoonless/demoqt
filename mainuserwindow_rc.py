# -*- coding: utf-8 -*-

# Resource object code
#
# Created: lun. nov. 25 18:46:33 2019
#      by: The Resource Compiler for PySide2 (Qt v5.13.2)
#
# WARNING! All changes made in this file will be lost!

from PySide2 import QtCore

qt_resource_data = b"\
\x00\x00\x14\xf8\
<\
?xml version=\x221.\
0\x22 encoding=\x22UTF\
-8\x22 standalone=\x22\
no\x22?>\x0a<!-- Creat\
ed with Inkscape\
 (http://www.ink\
scape.org/) -->\x0a\
<svg\x0a   xmlns:dc\
=\x22http://purl.or\
g/dc/elements/1.\
1/\x22\x0a   xmlns:cc=\
\x22http://web.reso\
urce.org/cc/\x22\x0a  \
 xmlns:rdf=\x22http\
://www.w3.org/19\
99/02/22-rdf-syn\
tax-ns#\x22\x0a   xmln\
s:svg=\x22http://ww\
w.w3.org/2000/sv\
g\x22\x0a   xmlns=\x22htt\
p://www.w3.org/2\
000/svg\x22\x0a   xmln\
s:sodipodi=\x22http\
://sodipodi.sour\
ceforge.net/DTD/\
sodipodi-0.dtd\x22\x0a\
   xmlns:inkscap\
e=\x22http://www.in\
kscape.org/names\
paces/inkscape\x22\x0a\
   width=\x2248px\x22\x0a\
   height=\x2248px\x22\
\x0a   id=\x22svg2160\x22\
\x0a   sodipodi:ver\
sion=\x220.32\x22\x0a   i\
nkscape:version=\
\x220.45\x22\x0a   sodipo\
di:modified=\x22tru\
e\x22>\x0a  <defs\x0a    \
 id=\x22defs2162\x22 /\
>\x0a  <sodipodi:na\
medview\x0a     id=\
\x22base\x22\x0a     page\
color=\x22#ffffff\x22\x0a\
     bordercolor\
=\x22#666666\x22\x0a     \
borderopacity=\x221\
.0\x22\x0a     inkscap\
e:pageopacity=\x220\
.0\x22\x0a     inkscap\
e:pageshadow=\x222\x22\
\x0a     inkscape:z\
oom=\x2215.041667\x22\x0a\
     inkscape:cx\
=\x2224\x22\x0a     inksc\
ape:cy=\x2224.95316\
6\x22\x0a     inkscape\
:current-layer=\x22\
layer1\x22\x0a     sho\
wgrid=\x22true\x22\x0a   \
  inkscape:grid-\
bbox=\x22true\x22\x0a    \
 inkscape:docume\
nt-units=\x22px\x22\x0a  \
   showguides=\x22t\
rue\x22\x0a     inksca\
pe:guide-bbox=\x22t\
rue\x22\x0a     inksca\
pe:grid-points=\x22\
true\x22\x0a     inksc\
ape:window-width\
=\x221280\x22\x0a     ink\
scape:window-hei\
ght=\x22945\x22\x0a     i\
nkscape:window-x\
=\x220\x22\x0a     inksca\
pe:window-y=\x2226\x22\
>\x0a    <sodipodi:\
guide\x0a       ori\
entation=\x22horizo\
ntal\x22\x0a       pos\
ition=\x224.9861496\
\x22\x0a       id=\x22gui\
de2168\x22 />\x0a    <\
sodipodi:guide\x0a \
      orientatio\
n=\x22vertical\x22\x0a   \
    position=\x225.\
0526316\x22\x0a       \
id=\x22guide2170\x22 /\
>\x0a    <sodipodi:\
guide\x0a       ori\
entation=\x22vertic\
al\x22\x0a       posit\
ion=\x2243.01385\x22\x0a \
      id=\x22guide2\
172\x22 />\x0a    <sod\
ipodi:guide\x0a    \
   orientation=\x22\
horizontal\x22\x0a    \
   position=\x2242.\
947368\x22\x0a       i\
d=\x22guide2174\x22 />\
\x0a  </sodipodi:na\
medview>\x0a  <meta\
data\x0a     id=\x22me\
tadata2165\x22>\x0a   \
 <rdf:RDF>\x0a     \
 <cc:Work\x0a      \
   rdf:about=\x22\x22>\
\x0a        <dc:for\
mat>image/svg+xm\
l</dc:format>\x0a  \
      <dc:type\x0a \
          rdf:re\
source=\x22http://p\
url.org/dc/dcmit\
ype/StillImage\x22 \
/>\x0a        <dc:t\
itle>User</dc:ti\
tle>\x0a        <dc\
:date>June 2007<\
/dc:date>\x0a      \
  <dc:creator>\x0a \
         <cc:Age\
nt>\x0a            \
<dc:title>Luca F\
erretti &lt;elle\
.uca@libero.it&g\
t;</dc:title>\x0a  \
        </cc:Age\
nt>\x0a        </dc\
:creator>\x0a      \
  <dc:subject>\x0a \
         <rdf:Ba\
g>\x0a            <\
rdf:li>user</rdf\
:li>\x0a           \
 <rdf:li>person<\
/rdf:li>\x0a       \
     <rdf:li>con\
tact</rdf:li>\x0a  \
        </rdf:Ba\
g>\x0a        </dc:\
subject>\x0a       \
 <cc:license\x0a   \
        rdf:reso\
urce=\x22http://cre\
ativecommons.org\
/licenses/LGPL/2\
.1/\x22 />\x0a      </\
cc:Work>\x0a      <\
cc:License\x0a     \
    rdf:about=\x22h\
ttp://creativeco\
mmons.org/licens\
es/LGPL/2.1/\x22>\x0a \
       <cc:permi\
ts\x0a           rd\
f:resource=\x22http\
://web.resource.\
org/cc/Reproduct\
ion\x22 />\x0a        \
<cc:permits\x0a    \
       rdf:resou\
rce=\x22http://web.\
resource.org/cc/\
Distribution\x22 />\
\x0a        <cc:req\
uires\x0a          \
 rdf:resource=\x22h\
ttp://web.resour\
ce.org/cc/Notice\
\x22 />\x0a        <cc\
:permits\x0a       \
    rdf:resource\
=\x22http://web.res\
ource.org/cc/Der\
ivativeWorks\x22 />\
\x0a        <cc:req\
uires\x0a          \
 rdf:resource=\x22h\
ttp://web.resour\
ce.org/cc/ShareA\
like\x22 />\x0a       \
 <cc:requires\x0a  \
         rdf:res\
ource=\x22http://we\
b.resource.org/c\
c/SourceCode\x22 />\
\x0a      </cc:Lice\
nse>\x0a    </rdf:R\
DF>\x0a  </metadata\
>\x0a  <g\x0a     id=\x22\
layer1\x22\x0a     ink\
scape:label=\x22Lay\
er 1\x22\x0a     inksc\
ape:groupmode=\x22l\
ayer\x22>\x0a    <rect\
\x0a       style=\x22o\
pacity:1;fill:no\
ne;fill-opacity:\
1;stroke:none;st\
roke-width:8;str\
oke-linecap:roun\
d;stroke-linejoi\
n:round;stroke-m\
iterlimit:4;stro\
ke-dasharray:non\
e;stroke-opacity\
:1\x22\x0a       id=\x22r\
ect8007\x22\x0a       \
width=\x2248\x22\x0a     \
  height=\x2248\x22\x0a  \
     x=\x220\x22\x0a     \
  y=\x220\x22\x0a       r\
y=\x220\x22 />\x0a    <pa\
th\x0a       sodipo\
di:nodetypes=\x22cc\
sccsc\x22\x0a       id\
=\x22path7036\x22\x0a    \
   d=\x22M 20,23 L \
28,23 C 34.65183\
7,23 40,27.81215\
3 40,34.46399 C \
40,37 40,38 40,4\
3 L 8,43 C 8,38 \
7.925209,38.4570\
64 7.925209,34.4\
6399 C 7.925209,\
27.812153 13.348\
163,23 20,23 z \x22\
\x0a       style=\x22f\
ill:#ffffff;fill\
-opacity:1;strok\
e:#ffffff;stroke\
-opacity:1;strok\
e-width:8;stroke\
-miterlimit:4;st\
roke-dasharray:n\
one;stroke-linej\
oin:round\x22 />\x0a  \
  <path\x0a       t\
ransform=\x22transl\
ate(1,2)\x22\x0a      \
 d=\x22M 33.864266 \
14 A 10.864266 1\
0.930748 0 1 1  \
12.135734,14 A 1\
0.864266 10.9307\
48 0 1 1  33.864\
266 14 z\x22\x0a      \
 sodipodi:ry=\x2210\
.930748\x22\x0a       \
sodipodi:rx=\x2210.\
864266\x22\x0a       s\
odipodi:cy=\x2214\x22\x0a\
       sodipodi:\
cx=\x2223\x22\x0a       i\
d=\x22path6065\x22\x0a   \
    style=\x22opaci\
ty:1;fill:#fffff\
f;fill-opacity:1\
;stroke:#ffffff;\
stroke-opacity:1\
;stroke-width:8;\
stroke-miterlimi\
t:4;stroke-dasha\
rray:none\x22\x0a     \
  sodipodi:type=\
\x22arc\x22 />\x0a    <pa\
th\x0a       style=\
\x22fill:#000000;fi\
ll-opacity:1;str\
oke:none\x22\x0a      \
 d=\x22M 20,23 L 28\
,23 C 34.651837,\
23 40,27.812153 \
40,34.46399 C 40\
,37 40,38 40,43 \
L 8,43 C 8,38 7.\
925209,38.457064\
 7.925209,34.463\
99 C 7.925209,27\
.812153 13.34816\
3,23 20,23 z \x22\x0a \
      id=\x22rect21\
76\x22\x0a       sodip\
odi:nodetypes=\x22c\
csccsc\x22 />\x0a    <\
path\x0a       styl\
e=\x22fill:#ffffff;\
fill-rule:evenod\
d;stroke:#ffffff\
;stroke-width:3;\
stroke-linecap:r\
ound;stroke-line\
join:round;strok\
e-opacity:1;fill\
-opacity:1;strok\
e-miterlimit:4;s\
troke-dasharray:\
none\x22\x0a       d=\x22\
M 18,23 L 24,31 \
L 29.518006,23 C\
 25.678671,23 21\
.839335,23 18,23\
\x22\x0a       id=\x22pat\
h3156\x22\x0a       so\
dipodi:nodetypes\
=\x22cccc\x22 />\x0a    <\
path\x0a       sodi\
podi:type=\x22arc\x22\x0a\
       style=\x22op\
acity:1;fill:#00\
0000;fill-opacit\
y:1;stroke:none\x22\
\x0a       id=\x22path\
3154\x22\x0a       sod\
ipodi:cx=\x2223\x22\x0a  \
     sodipodi:cy\
=\x2214\x22\x0a       sod\
ipodi:rx=\x2210.864\
266\x22\x0a       sodi\
podi:ry=\x2210.9307\
48\x22\x0a       d=\x22M \
33.864266 14 A 1\
0.864266 10.9307\
48 0 1 1  12.135\
734,14 A 10.8642\
66 10.930748 0 1\
 1  33.864266 14\
 z\x22\x0a       trans\
form=\x22translate(\
1,2)\x22 />\x0a  </g>\x0a\
</svg>\x0a\
"

qt_resource_name = b"\
\x00\x05\
\x00o\xa6S\
\x00i\
\x00c\x00o\x00n\x00s\
\x00\x08\
\x07e\xf7^\
\x00a\
\x00p\x00p\x00_\x00i\x00c\x00o\x00n\
"

qt_resource_struct = b"\
\x00\x00\x00\x00\x00\x02\x00\x00\x00\x01\x00\x00\x00\x01\
\x00\x00\x00\x00\x00\x02\x00\x00\x00\x01\x00\x00\x00\x02\
\x00\x00\x00\x10\x00\x00\x00\x00\x00\x01\x00\x00\x00\x00\
"

def qInitResources():
    QtCore.qRegisterResourceData(0x01, qt_resource_struct, qt_resource_name, qt_resource_data)

def qCleanupResources():
    QtCore.qUnregisterResourceData(0x01, qt_resource_struct, qt_resource_name, qt_resource_data)

qInitResources()
