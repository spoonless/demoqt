# This Python file uses the following encoding: utf-8
import os
from PySide2.QtCore import Signal
from PySide2.QtWidgets import QApplication, QMainWindow, QMessageBox, QErrorMessage, QFileDialog
from PySide2.QtGui import QPixmap
from mainwindow_ui import Ui_MainWindow


class ErrorHandlerMixin:

    error = Signal(str)

    def __init__(self, *args, **kwargs):
        super().__init__(*args, **kwargs)
        self._errorMessage = QErrorMessage(self)
        self.error.connect(self._errorMessage.showMessage)


class MainWindow(ErrorHandlerMixin, QMainWindow):

    def __init__(self, parent=None):
        super().__init__(parent)
        self.ui = Ui_MainWindow()
        self.ui.setupUi(self)

    def open(self):
        filename, selected_filter = QFileDialog.getOpenFileName(self, "Sélectionner une image", os.getcwd(), "Images (*.png *.jpg *.jpeg *.bmp)")
        if filename:
            pixmap = QPixmap()
            pixmap.load(filename)
            self.ui.image.setPixmap(pixmap)

    def quit(self):
        QApplication.instance().closeAllWindows()

    def closeEvent(self, event):
        answer = QMessageBox.question(self, "Quitter", "Voulez-vous quitter l'application ?")
        if answer == QMessageBox.Yes:
            event.accept()
        else:
            event.ignore()


if __name__ == "__main__":
    import sys
    app = QApplication(sys.argv)
    window = MainWindow()
    window.show()
    app.exec_()
