# This Python file uses the following encoding: utf-8
import sys
from PySide2.QtWidgets import QApplication
from PySide2.QtSql import QSqlDatabase, QSqlQuery


class UtilisateurDao:

    @classmethod
    def get_all(cls):
        q = QSqlQuery()
        q.exec_("""select login, dateInscription, actif
                   from Utilisateur""")
        utilisateurs = []
        while q.next():
            utilisateurs.append(cls.create_utilisateur(q))

        return utilisateurs


    @classmethod
    def get_by_login(cls, login):
        q = QSqlQuery()
        q.prepare("""select login, dateInscription, actif
                     from Utilisateur
                     where login = :login""")
        q.bindValue(":login", login)
        q.exec_()
        return cls.create_utilisateur(q) if q.next() else None


    @classmethod
    def create_utilisateur(cls, query):
        return {
            'login': query.value('login'),
            'dateInscription': query.value('dateInscription').toPython(),
            'actif': query.value('actif') > 0
        }


if __name__ == "__main__":

    app = QApplication([])

    db = QSqlDatabase.addDatabase('QMYSQL')
    db.setHostName('10.142.140.211')
    db.setPort(3306)
    db.setDatabaseName("udev3")

    if db.open("david", "david"):
        print("Liste des utilisateurs :")
        for u in UtilisateurDao.get_all():
            print(f"{u['login']} {u['dateInscription']} {u['actif']}")

        login = input("Saisissez un login d'utilisateur : ")
        u = UtilisateurDao.get_by_login(login)
        print("L'utilisateur existe" if u is not None else "L'utilisateur n'existe pas")
        db.close()

