# This Python file uses the following encoding: utf-8
from PySide2.QtWidgets import QApplication, QDialog
import infodialog_ui

class Info:

    def __str__(self):
        return f"{self.prenom} {self.nom}"


class InfoDialog(QDialog):

    def __init__(self, parent=None):
        super().__init__(parent)
        self.ui = infodialog_ui.Ui_Dialog()
        self.ui.setupUi(self)

    def accept(self):
        self.info = Info()
        self.info.prenom = self.ui.prenom.text()
        self.info.nom = self.ui.nom.text()
        super().accept()


if __name__ == "__main__":
    app = QApplication([])
    dlg = InfoDialog()
    dlg.exec_()
    if (dlg.result() == QDialog.Accepted):
        print(f"Bonjour {dlg.info}")
