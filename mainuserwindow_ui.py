# -*- coding: utf-8 -*-

# Form implementation generated from reading ui file 'mainuserwindow.ui',
# licensing of 'mainuserwindow.ui' applies.
#
# Created: Mon Nov 25 19:20:25 2019
#      by: pyside2-uic  running on PySide2 5.13.2
#
# WARNING! All changes made in this file will be lost!

from PySide2 import QtCore, QtGui, QtWidgets

class Ui_MainWindow(object):
    def setupUi(self, MainWindow):
        MainWindow.setObjectName("MainWindow")
        MainWindow.resize(800, 600)
        icon = QtGui.QIcon()
        icon.addPixmap(QtGui.QPixmap(":/icons/app_icon"), QtGui.QIcon.Normal, QtGui.QIcon.Off)
        MainWindow.setWindowIcon(icon)
        self.centralwidget = QtWidgets.QWidget(MainWindow)
        self.centralwidget.setObjectName("centralwidget")
        self.verticalLayout = QtWidgets.QVBoxLayout(self.centralwidget)
        self.verticalLayout.setObjectName("verticalLayout")
        self.datatable = QtWidgets.QTableView(self.centralwidget)
        self.datatable.setFrameShadow(QtWidgets.QFrame.Plain)
        self.datatable.setObjectName("datatable")
        self.verticalLayout.addWidget(self.datatable)
        MainWindow.setCentralWidget(self.centralwidget)
        self.menubar = QtWidgets.QMenuBar(MainWindow)
        self.menubar.setGeometry(QtCore.QRect(0, 0, 800, 22))
        self.menubar.setObjectName("menubar")
        self.menu_File = QtWidgets.QMenu(self.menubar)
        self.menu_File.setObjectName("menu_File")
        self.menu_Aide = QtWidgets.QMenu(self.menubar)
        self.menu_Aide.setObjectName("menu_Aide")
        MainWindow.setMenuBar(self.menubar)
        self.statusbar = QtWidgets.QStatusBar(MainWindow)
        self.statusbar.setEnabled(True)
        self.statusbar.setObjectName("statusbar")
        MainWindow.setStatusBar(self.statusbar)
        self.action_connection = QtWidgets.QAction(MainWindow)
        self.action_connection.setObjectName("action_connection")
        self.action_quit = QtWidgets.QAction(MainWindow)
        self.action_quit.setObjectName("action_quit")
        self.action_about = QtWidgets.QAction(MainWindow)
        self.action_about.setObjectName("action_about")
        self.action_about_qt = QtWidgets.QAction(MainWindow)
        self.action_about_qt.setObjectName("action_about_qt")
        self.menu_File.addAction(self.action_connection)
        self.menu_File.addSeparator()
        self.menu_File.addAction(self.action_quit)
        self.menu_Aide.addAction(self.action_about_qt)
        self.menu_Aide.addAction(self.action_about)
        self.menubar.addAction(self.menu_File.menuAction())
        self.menubar.addAction(self.menu_Aide.menuAction())

        self.retranslateUi(MainWindow)
        QtCore.QObject.connect(self.action_about, QtCore.SIGNAL("triggered()"), MainWindow.about)
        QtCore.QObject.connect(self.action_quit, QtCore.SIGNAL("triggered()"), MainWindow.quit)
        QtCore.QObject.connect(self.action_connection, QtCore.SIGNAL("triggered()"), MainWindow.openConnection)
        QtCore.QObject.connect(self.action_about_qt, QtCore.SIGNAL("triggered()"), MainWindow.aboutQt)
        QtCore.QMetaObject.connectSlotsByName(MainWindow)

    def retranslateUi(self, MainWindow):
        MainWindow.setWindowTitle(QtWidgets.QApplication.translate("MainWindow", "Utilisateurs", None, -1))
        self.menu_File.setTitle(QtWidgets.QApplication.translate("MainWindow", "&File", None, -1))
        self.menu_Aide.setTitle(QtWidgets.QApplication.translate("MainWindow", "&Aide", None, -1))
        self.action_connection.setText(QtWidgets.QApplication.translate("MainWindow", "&Connexion...", None, -1))
        self.action_quit.setText(QtWidgets.QApplication.translate("MainWindow", "&Quitter", None, -1))
        self.action_quit.setShortcut(QtWidgets.QApplication.translate("MainWindow", "Ctrl+Q", None, -1))
        self.action_about.setText(QtWidgets.QApplication.translate("MainWindow", "À propos...", None, -1))
        self.action_about_qt.setText(QtWidgets.QApplication.translate("MainWindow", "À propos de Qt...", None, -1))

import mainuserwindow_rc
