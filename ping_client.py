#! /usr/bin/env python3

import socket

def ping_client(host, port, message):
    with socket.socket(socket.AF_INET, socket.SOCK_STREAM) as s:
        s.connect((host, port))
        s.sendall(message.encode('utf-8'))
        buff = s.recv(512)
        print(buff.decode())

if __name__ == "__main__":
    import argparse

    parser = argparse.ArgumentParser()
    parser.add_argument('-H', '--host', default='localhost', help="l'adresse ou le nom d'hôte du serveur")
    parser.add_argument('-p', '--port', type=int, default=8000, help="le port d'écoute du serveur")
    parser.add_argument('message', default="ping", help="le message à envoyer au serveur")
    args = parser.parse_args()

    ping_client(args.host, args.port, args.message)

