# -*- coding: utf-8 -*-

# Form implementation generated from reading ui file 'connectiondialog.ui',
# licensing of 'connectiondialog.ui' applies.
#
# Created: Tue Dec  3 13:54:37 2019
#      by: pyside2-uic  running on PySide2 5.13.2
#
# WARNING! All changes made in this file will be lost!

from PySide2 import QtCore, QtGui, QtWidgets

class Ui_ConnexionDialog(object):
    def setupUi(self, ConnexionDialog):
        ConnexionDialog.setObjectName("ConnexionDialog")
        ConnexionDialog.resize(360, 161)
        self.verticalLayout = QtWidgets.QVBoxLayout(ConnexionDialog)
        self.verticalLayout.setObjectName("verticalLayout")
        self.label_4 = QtWidgets.QLabel(ConnexionDialog)
        self.label_4.setObjectName("label_4")
        self.verticalLayout.addWidget(self.label_4)
        self.formLayout = QtWidgets.QFormLayout()
        self.formLayout.setObjectName("formLayout")
        self.label = QtWidgets.QLabel(ConnexionDialog)
        self.label.setObjectName("label")
        self.formLayout.setWidget(0, QtWidgets.QFormLayout.LabelRole, self.label)
        self.label_2 = QtWidgets.QLabel(ConnexionDialog)
        self.label_2.setObjectName("label_2")
        self.formLayout.setWidget(1, QtWidgets.QFormLayout.LabelRole, self.label_2)
        self.label_3 = QtWidgets.QLabel(ConnexionDialog)
        self.label_3.setObjectName("label_3")
        self.formLayout.setWidget(2, QtWidgets.QFormLayout.LabelRole, self.label_3)
        self.dburl = QtWidgets.QLineEdit(ConnexionDialog)
        self.dburl.setObjectName("dburl")
        self.formLayout.setWidget(0, QtWidgets.QFormLayout.FieldRole, self.dburl)
        self.login = QtWidgets.QLineEdit(ConnexionDialog)
        self.login.setInputMask("")
        self.login.setEchoMode(QtWidgets.QLineEdit.Normal)
        self.login.setObjectName("login")
        self.formLayout.setWidget(1, QtWidgets.QFormLayout.FieldRole, self.login)
        self.password = QtWidgets.QLineEdit(ConnexionDialog)
        self.password.setEchoMode(QtWidgets.QLineEdit.Password)
        self.password.setObjectName("password")
        self.formLayout.setWidget(2, QtWidgets.QFormLayout.FieldRole, self.password)
        self.verticalLayout.addLayout(self.formLayout)
        self.buttonBox = QtWidgets.QDialogButtonBox(ConnexionDialog)
        self.buttonBox.setOrientation(QtCore.Qt.Horizontal)
        self.buttonBox.setStandardButtons(QtWidgets.QDialogButtonBox.Cancel|QtWidgets.QDialogButtonBox.Ok)
        self.buttonBox.setObjectName("buttonBox")
        self.verticalLayout.addWidget(self.buttonBox)
        self.label.setBuddy(self.dburl)
        self.label_2.setBuddy(self.login)
        self.label_3.setBuddy(self.password)

        self.retranslateUi(ConnexionDialog)
        QtCore.QObject.connect(self.buttonBox, QtCore.SIGNAL("accepted()"), ConnexionDialog.accept)
        QtCore.QObject.connect(self.buttonBox, QtCore.SIGNAL("rejected()"), ConnexionDialog.reject)
        QtCore.QMetaObject.connectSlotsByName(ConnexionDialog)

    def retranslateUi(self, ConnexionDialog):
        ConnexionDialog.setWindowTitle(QtWidgets.QApplication.translate("ConnexionDialog", "Connexion", None, -1))
        self.label_4.setText(QtWidgets.QApplication.translate("ConnexionDialog", "Information de connexion à la base de données", None, -1))
        self.label.setText(QtWidgets.QApplication.translate("ConnexionDialog", "&URL", None, -1))
        self.label_2.setText(QtWidgets.QApplication.translate("ConnexionDialog", "&Login", None, -1))
        self.label_3.setText(QtWidgets.QApplication.translate("ConnexionDialog", "&Mot de passe", None, -1))

