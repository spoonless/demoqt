# This Python file uses the following encoding: utf-8
from PySide2.QtCore import Signal, Slot, QUrl, Qt
from PySide2.QtWidgets import QApplication, QMainWindow, QDialog, QMessageBox, QErrorMessage
from PySide2.QtSql import QSqlDatabase, QSqlTableModel
from mainuserwindow_ui import Ui_MainWindow
from connectiondialog_ui import Ui_ConnexionDialog

import mainuserwindow_rc

class ErrorHandlerMixin:

    error = Signal(str)

    def __init__(self, *args, **kwargs):
        super().__init__(*args, **kwargs)
        self._errorMessage = QErrorMessage(self)
        self.error.connect(self._errorMessage.showMessage)


class ConnectionDialog(ErrorHandlerMixin, QDialog):

    ALL_DRIVERS = {
        "sqlite": "QSQLITE",
        "mysql": "QMYSQL",
        "psql": "QPSQL",
        "psql7": "QPSQL7"
    }

    dbConnected = Signal()
    dbDisconnected = Signal()

    def __init__(self, parent=None):
        super().__init__(parent)
        self.setModal(True)
        self.ui = Ui_ConnexionDialog()
        self.ui.setupUi(self)
        self.driversDict = self.createDriversDict();

        self.ui.dburl.setText("mysql://10.142.140.211:3306/udev3")
        self.ui.login.setText("david")
        self.ui.password.setText("david")

    def accept(self):
        url = QUrl(self.ui.dburl.text())
        if not self.validateUrl(url):
            return
        if not self.openConnection(url, self.ui.login.text(), self.ui.password.text()):
            return
        super().accept()

    def createDriversDict(self):
        drivers = {}
        for s, d in ConnectionDialog.ALL_DRIVERS.items():
            if QSqlDatabase.isDriverAvailable(d):
                drivers[s] = d
        return drivers

    def openConnection(self, url, login, password):
        self.closeConnection()
        db = QSqlDatabase.addDatabase(self.driversDict[url.scheme()])
        db.setHostName(url.host())
        db.setPort(url.port())
        db.setDatabaseName(url.fileName())
        db.open(login, password)
        if db.isOpenError():
            self.error.emit(f"Erreur de connexion : {db.lastError().text()}")
            return False
        self.dbConnected.emit()
        return True

    def closeConnection(self):
        lastConnection = QSqlDatabase.database()
        if lastConnection.isOpen():
            self.dbDisconnected.emit()
            lastConnection.close()

    def validateUrl(self, url):
        if not url.isValid():
            self.error.emit("L'URL de connexion à la base de données est invalide !")
            return False
        if url.scheme() not in self.driversDict:
            self.error.emit(f"Le schéma \"{url.scheme()}\" dans l'URL de connexion est inconnu !")
            return False
        if url.scheme() != 'sqlite' and not url.host():
            self.error.emit("Aucun hôte spécifié dans l'URL de connexion !")
            return False
        if not url.fileName():
            self.error.emit("Aucun schéma de base de données spécifié dans l'URL de connexion !")
            return False
        return True


class MainUserWindow(QMainWindow):

    def __init__(self, parent=None):
        super().__init__(parent)
        self.ui = Ui_MainWindow()
        self.ui.setupUi(self)

    def openConnection(self):
        dialog = ConnectionDialog(self)
        dialog.dbConnected.connect(self.fetchData)
        dialog.dbDisconnected.connect(self.clearData)
        dialog.show()

    def clearData(self):
        self.ui.datatable.setModel(None)

    def fetchData(self):
        model = QSqlTableModel()
        model.setTable("Utilisateur")
        model.select()
        model.removeColumn(0)
        model.setHeaderData(0, Qt.Horizontal, "Nom")
        model.setHeaderData(1, Qt.Horizontal, "Date inscription")
        model.setHeaderData(2, Qt.Horizontal, "Actif")
        self.ui.datatable.setModel(model)

    def quit(self):
        QApplication.instance().closeAllWindows()

    def about(self):
        QMessageBox.about(self, "À propos", "Une application de démo avec PySide2.")

    def aboutQt(self):
        QMessageBox.aboutQt(self)

    def closeEvent(self, event):
        answer = QMessageBox.question(self, "Quitter", "Voulez-vous quitter l'application ?")
        if answer == QMessageBox.Yes:
            event.accept()
        else:
            event.ignore()


if __name__ == "__main__":
    import sys
    app = QApplication(sys.argv)
    window = MainUserWindow()
    window.show()
    app.exec_()
