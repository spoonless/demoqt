from PySide2.QtWidgets import QApplication, QMainWindow
from PySide2.QtGui import QPainter
from PySide2.QtCharts import QtCharts

def create_series():
    import random
    series = QtCharts.QPieSeries()
    for j in ('Lundi', 'Mardi', 'Mercredi', 'Jeudi', 'Vendredi'):
        series.append(j, random.randint(0, 100))
    return series


def create_series2():
    import random
    series = QtCharts.QLineSeries()
    for n in range (0, 100):
        series.append(n, random.randrange(-10, 10))
    return series


def create_chart():
    chart = QtCharts.QChart()
    chart.addSeries(create_series())
    chart.createDefaultAxes()

    view = QtCharts.QChartView(chart)
    view.setRenderHint(QPainter.Antialiasing)
    return view


if __name__ == "__main__":
    app = QApplication([])

    window = QMainWindow()
    window.setCentralWidget(create_chart())
    window.show()
    app.exec_()
