from PySide2.QtCore import Slot
from PySide2.QtWidgets import QApplication, QWidget, QMenu
import widgetwithcontextmenu_ui

class FormWidget(QWidget):

    def __init__(self, parent=None):
        super().__init__(parent)
        self.ui = widgetwithcontextmenu_ui.Ui_Form()
        self.ui.setupUi(self)

    @Slot()
    def on_button_clicked(self):
        QApplication.instance().closeAllWindows()

    @Slot()
    def on_action_copier_triggered(self):
        print("copier")

    @Slot()
    def on_action_coller_triggered(self):
        print("coller")

    @Slot()
    def on_action_detruire_triggered(self):
        print("détruire")

    @Slot()
    def on_action_fermer_triggered(self):
        print("fermer")

    def ctxMenu(self, point):
        menu = QMenu()
        menu.addAction(self.ui.action_copier)
        menu.addAction(self.ui.action_coller)
        menu.addSeparator()
        menu.addAction(self.ui.action_detruire)
        menu.addSeparator()
        menu.addAction(self.ui.action_fermer)
        menu.exec_(self.mapToGlobal(point));


if __name__ == "__main__":
    app = QApplication([])
    w = FormWidget()
    w.show()
    app.exec_()
