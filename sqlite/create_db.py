import sqlite3

conn = sqlite3.connect('utilisateurs.db')

c = conn.cursor()
c.execute("""
create table utilisateur(
    id integer primary key autoincrement,
    login varchar(100),
    dateInscription date
);
""")

conn.close()
