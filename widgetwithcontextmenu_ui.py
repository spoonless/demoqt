# -*- coding: utf-8 -*-

# Form implementation generated from reading ui file 'widgetwithcontextmenu.ui',
# licensing of 'widgetwithcontextmenu.ui' applies.
#
# Created: Sun Dec  1 23:41:08 2019
#      by: pyside2-uic  running on PySide2 5.13.2
#
# WARNING! All changes made in this file will be lost!

from PySide2 import QtCore, QtGui, QtWidgets

class Ui_Form(object):
    def setupUi(self, Form):
        Form.setObjectName("Form")
        Form.resize(400, 300)
        Form.setContextMenuPolicy(QtCore.Qt.CustomContextMenu)
        self.button = QtWidgets.QPushButton(Form)
        self.button.setGeometry(QtCore.QRect(160, 130, 80, 25))
        self.button.setContextMenuPolicy(QtCore.Qt.PreventContextMenu)
        self.button.setObjectName("button")
        self.action_copier = QtWidgets.QAction(Form)
        self.action_copier.setObjectName("action_copier")
        self.action_coller = QtWidgets.QAction(Form)
        self.action_coller.setObjectName("action_coller")
        self.action_detruire = QtWidgets.QAction(Form)
        self.action_detruire.setObjectName("action_detruire")
        self.action_fermer = QtWidgets.QAction(Form)
        self.action_fermer.setObjectName("action_fermer")

        self.retranslateUi(Form)
        QtCore.QObject.connect(Form, QtCore.SIGNAL("customContextMenuRequested(QPoint)"), Form.ctxMenu)
        QtCore.QMetaObject.connectSlotsByName(Form)

    def retranslateUi(self, Form):
        Form.setWindowTitle(QtWidgets.QApplication.translate("Form", "Form", None, -1))
        self.button.setText(QtWidgets.QApplication.translate("Form", "Quitter", None, -1))
        self.action_copier.setText(QtWidgets.QApplication.translate("Form", "copier", None, -1))
        self.action_coller.setText(QtWidgets.QApplication.translate("Form", "coller", None, -1))
        self.action_detruire.setText(QtWidgets.QApplication.translate("Form", "détruire", None, -1))
        self.action_fermer.setText(QtWidgets.QApplication.translate("Form", "fermer", None, -1))

