# This Python file uses the following encoding: utf-8
import sys
from PySide2.QtCore import QObject, QStateMachine, QState, Signal, Property, Slot

class Fridge(QObject):

    temperature_changed = Signal(int)

    def __init__(self, temperature = 3):
        super().__init__()
        self.temperature = temperature

    @Property(int, notify=temperature_changed)
    def temperature(self):
        return self._temperature

    @temperature.setter
    def set_temperature(self, t):
        self._temperature = t
        self.temperature_changed.emit(self._temperature)

    @Slot(int)
    def change_temperature(self, t):
        self.temperature = t

class Thermostat(QObject):

    refrigerate = Signal(int)

    def __init__(self, temperature_min = 3):
        super().__init__()
        self.temperature_min = temperature_min

    @Slot(int)
    def check_temperature(self, t):
        if (t > self.temperature_min):
            self.refrigerate.emit(self.temperature_min)

if __name__ == "__main__":
    fridge = Fridge()
    thermostat = Thermostat()
    thermostat.refrigerate.connect(fridge.change_temperature)
    fridge.temperature_changed.connect(thermostat.check_temperature)

    fridge.temperature = 12
    print(fridge.temperature)
